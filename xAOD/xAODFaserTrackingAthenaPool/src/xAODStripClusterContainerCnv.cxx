// System include(s):
#include <exception>

// Local include(s):
#include "xAODStripClusterContainerCnv.h"

namespace {

   /// Helper function setting up the container's link to its auxiliary store
   void setStoreLink( SG::AuxVectorBase* cont, const std::string& key ) {

      // The link to set up:
      DataLink< SG::IConstAuxStore > link( key + "Aux." );

      // Give it to the container:
      cont->setStore( link );

      return;
   }

} // private namespace

xAODStripClusterContainerCnv::xAODStripClusterContainerCnv( ISvcLocator* svcLoc )
   : xAODStripClusterContainerCnvBase( svcLoc ) {
}

xAOD::StripClusterContainer*
xAODStripClusterContainerCnv::
createPersistent( xAOD::StripClusterContainer* trans ) {

   // Create a view copy of the container:
   xAOD::StripClusterContainer* result =
      new xAOD::StripClusterContainer( trans->begin(), trans->end(),
                                      SG::VIEW_ELEMENTS );

   // Return the new container:
   return result;
}

/**
 * This function needs to be re-implemented in order to figure out the StoreGate
 * key of the container that's being created. After that's done, it lets the
 * base class do its normal task.
 */
StatusCode xAODStripClusterContainerCnv::createObj( IOpaqueAddress* pAddr,
                                                   DataObject*& pObj ) {

   // Get the key of the container that we'll be creating:
   m_key = *( pAddr->par() + 1 );
   ATH_MSG_VERBOSE( "Key of xAOD::StripClusterContainer: " << m_key );

   // Let the base class do its thing now:
   return AthenaPoolConverter::createObj( pAddr, pObj );
}

xAOD::StripClusterContainer* xAODStripClusterContainerCnv::createTransient() {

   // The known ID(s) for this container:
   static pool::Guid v1_guid( "868F1FD8-A5E7-4B40-B84E-73716C37A6B0" );

   // Check if we're reading the most up to date type:
   if( compareClassGuid( v1_guid ) ) {
      xAOD::StripClusterContainer* c = poolReadObject< xAOD::StripClusterContainer >();
      setStoreLink( c, m_key );
      return c;
   }

   // If we didn't recognise the ID, let's complain:
   throw std::runtime_error( "Unsupported version of "
                             "xAOD::StripClusterContainer found" );
   return 0;
}
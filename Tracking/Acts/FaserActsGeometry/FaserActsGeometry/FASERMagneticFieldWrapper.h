/*
   Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
   */

#ifndef FASERACTSGEOMETRY_FASERMAGNETICFIELDWRAPPER_H
#define FASERACTSGEOMETRY_FASERMAGNETICFIELDWRAPPER_H

#include "GaudiKernel/PhysicalConstants.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "MagFieldElements/FaserFieldCache.h"
#include "Acts/Utilities/Definitions.hpp"
#include "Acts/Utilities/Units.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"

class FASERMagneticFieldWrapper
{


  public:

    struct Cache {

      Cache(std::reference_wrapper<const Acts::MagneticFieldContext> mctx) {
        std::any_cast<const FaserFieldCacheCondObj*>(mctx)->getInitializedCache(fieldCache);
      }

      MagField::FaserFieldCache fieldCache;
    };


    FASERMagneticFieldWrapper() = default;

    Acts::Vector3D
    getField(const Acts::Vector3D& pos, Cache& cache) const
      {
	double pos0[]{pos.x(), pos.y(), pos.z()};
	double bfield0[]{0., 0., 0.};
	cache.fieldCache.getField(pos0, bfield0);
	Acts::Vector3D bfield(bfield0[0], bfield0[1], bfield0[2]);

	bfield *= m_bFieldUnit; // kT -> T;

	return bfield;
      }

    Acts::Vector3D
    getFieldGradient(const Acts::Vector3D& position, Acts::ActsMatrixD<3, 3>& gradient, Cache& cache) const
      {
	double position0[]{position.x(), position.y(), position.z()};
	double bfield0[]{0., 0., 0.};
	double grad[9];
	cache.fieldCache.getField(position0, bfield0, grad);
	Acts::Vector3D bfield(bfield0[0], bfield0[1], bfield0[2]);
        Acts::ActsMatrixD<3, 3> tempGrad;
	tempGrad << grad[0], grad[1], grad[2], grad[3], grad[4], grad[5], grad[6], grad[7], grad[8]; 
	gradient = tempGrad;

	bfield *= m_bFieldUnit; // kT -> T;
	gradient *= m_bFieldUnit;

	return bfield;
      }


  private:
    const double m_bFieldUnit = 1000.*Acts::UnitConstants::T;
};


#endif

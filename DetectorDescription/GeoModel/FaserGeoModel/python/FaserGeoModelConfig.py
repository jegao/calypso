#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
# from AthenaConfiguration.ComponentFactory import CompFactory

def FaserGeometryCfg (flags):
    acc = ComponentAccumulator()

    from FaserGeoModel.ScintGMConfig import ScintGeometryCfg
    acc.merge(ScintGeometryCfg(flags))

    from FaserGeoModel.SCTGMConfig import SctGeometryCfg
    acc.merge(SctGeometryCfg(flags))

    from FaserGeoModel.DipoleGMConfig import DipoleGeometryCfg
    acc.merge(DipoleGeometryCfg(flags))

    # Hack to avoid randomly failing to find sqlite file
    # poolSvc = CompFactory.PoolSvc()
    # poolSvc.SortReplicas = False
    # acc.addService(poolSvc)

    # if flags.Detector.SimulateScintillator or flags.Detector.GeometryScintillator:
    #     from FaserGeoModel.ScintGMConfig import ScintGeometryCfg
    #     acc.merge(ScintGeometryCfg(flags))

    # if flags.Detector.SimulateTracker or flags.Detector.GeometryTracker:
    #     from FaserGeoModel.SCTGMConfig import SctGeometryCfg
    #     acc.merge(SctGeometryCfg(flags))

    # if flags.Detector.SimulateDipole or flags.Detector.GeometryDipole:
    #     from FaserGeoModel.DipoleGMConfig import DipoleGeometryCfg
    #     acc.merge(DipoleGeometryCfg(flags))

    return acc

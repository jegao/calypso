#ifndef FASERTESTCONVERTER_H
#define FASERTESTCONVERTER_H

//STL
#include <string>

// ROOT
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

// Base class
#include "AthenaBaseComps/AthAlgorithm.h"

#include "LegacyBase/FaserEvent.h"

namespace Legacy{
class FaserTestConverter : public AthAlgorithm {
public:
  /// Constructor with parameters:
  FaserTestConverter(const std::string &name,ISvcLocator *pSvcLocator);
  
  ~FaserTestConverter();
  
  StatusCode initialize();
  StatusCode execute();
  StatusCode finalize();

private:

  std::string m_inputFileName;
  std::string m_treeName;
  std::string m_branchName;

  std::string m_truthEventCollectionName;
  std::string m_truthParticleCollectionName;
  std::string m_truthVertexCollectionName;

  std::string m_clusCollectionName; 

  TFile* m_tfile;
  TTree* m_ttree;
  TBranch* m_tbranch;

  FaserEvent* m_faserEvent;
  long m_currentEvent;
  long m_totalEvents;

  StatusCode fillTrackerClusters() const;
  StatusCode fillTruth() const;
};


} // End of namespace

#endif // FASERTESTCONVERTER_H

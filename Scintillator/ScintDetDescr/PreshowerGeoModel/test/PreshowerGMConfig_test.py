#!/usr/bin/env python
"""Run tests on VetoGeoModel configuration

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior=1
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags
    from AthenaConfiguration.TestDefaults import defaultTestFiles

    ConfigFlags.Input.Files = defaultTestFiles.HITS
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"
    # ConfigFlags.Detector.SimulatePixel = False
    ConfigFlags.Detector.SimulatePreshower = False
    # ConfigFlags.Detector.SimulateTRT   = False
    ConfigFlags.GeoModel.Align.Dynamic    = False
    ConfigFlags.lock()

    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    from PreshowerGeoModel.PreshowerGeoModelConfig import PreshowerGeometryCfg
    acc = PreshowerGeometryCfg(ConfigFlags)
    f=open('PreshowerGeometryCfg.pkl','wb')
    acc.store(f)
    f.close()

//Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#ifndef SCINTBYTESTREAM_FASERTRIGGERDECODERTOOL_H
#define SCINTBYTESTREAM_FASERTRIGGERDECODERTOOL_H

#include "AthenaBaseComps/AthAlgTool.h"
#include "GaudiKernel/ToolHandle.h"

#include "EventFormats/DAQFormats.hpp"
#include "ScintRawEvent/ScintWaveformContainer.h"

// This class provides conversion between bytestream and Waveform objects

class ScintWaveformDecoderTool : public AthAlgTool {

 public:
  ScintWaveformDecoderTool(const std::string& type, const std::string& name, 
			  const IInterface* parent);

  virtual ~ScintWaveformDecoderTool();

  static const InterfaceID& interfaceID();

  virtual StatusCode initialize();
  virtual StatusCode finalize();

  StatusCode convert(const DAQFormats::EventFull* re, ScintWaveformContainer* wfm, std::string key);

private:
  // List of channels to include in each container
  std::vector<unsigned int> m_caloChannels;
  std::vector<unsigned int> m_vetoChannels;
  std::vector<unsigned int> m_triggerChannels;
  std::vector<unsigned int> m_preshowerChannels;
  std::vector<unsigned int> m_testChannels;
  std::vector<unsigned int> m_clockChannels;


};

#endif  /* SCINTBYTESTREAM_FASERTRIGGERDECODERTOOL_H */
 

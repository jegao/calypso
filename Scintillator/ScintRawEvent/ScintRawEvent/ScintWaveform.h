/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// ScintWaveform.h
//   Header file for class ScintWaveform
///////////////////////////////////////////////////////////////////
// Class for the raw waveform data for all scintillator detectors
// This is a direct conversion of the DigitizerDataFragment data
///////////////////////////////////////////////////////////////////

#ifndef SCINTRAWEVENT_SCINTWAVEFORM_H
#define SCINTRAWEVENT_SCINTWAVEFORM_H

#include <bitset>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "AthenaKernel/CLASS_DEF.h"

class DigitizerDataFragment;

class ScintWaveform  {

  ///////////////////////////////////////////////////////////////////
  // Public methods:
  ///////////////////////////////////////////////////////////////////
public:

  // Default constructor
  ScintWaveform();

  // Destructor:
  virtual ~ScintWaveform(); 
  
  //move assignment defaulted
  ScintWaveform & operator = (ScintWaveform &&) = default;
  //assignment defaulted
  ScintWaveform & operator = (const ScintWaveform &) = default;
  //copy c'tor defaulted
  ScintWaveform(const ScintWaveform &) = default;

  ///////////////////////////////////////////////////////////////////
  // Const methods:
  ///////////////////////////////////////////////////////////////////

  // General data from Digitizer
  unsigned int board_id() const;
  bool         board_fail_flag() const;
  unsigned int pattern_trig_options() const;
  unsigned int channel_mask() const;
  unsigned int event_counter() const;
  unsigned int trigger_time_tag() const;
  unsigned int n_samples() const;

  // Waveform data
  unsigned int channel() const;
  const std::vector<unsigned int>& adc_counts() const;

  unsigned int identify() const;

  // some print-out:
  void print() const;

  bool operator < (const ScintWaveform& rhs) const
  {return m_ID < rhs.m_ID;}

  // Set functions
  void setIdentifier (unsigned int id);
  void setHeader (const DigitizerDataFragment* frag);
  void setWaveform (unsigned int channel, const std::vector<uint16_t> waveform);

  // Set functions for P->T conversion
  void setBoardId(unsigned int id) {m_board_id = id;}
  void setBoardFailFlag(bool flag) {m_board_fail_flag = flag;}
  void setPatternTrigOptions(unsigned int pattern) {m_pattern_trig_options = pattern;}
  void setChannelMask(unsigned int mask) {m_channel_mask = mask;}
  void setEventCounter(unsigned int counter) {m_event_counter = counter;}
  void setTriggerTime(unsigned int tag) {m_trigger_time_tag = tag;}
  void setChannel(unsigned int chan) {m_channel = chan;}
  void setSamples(unsigned int samp) {m_samples = samp;}
  void setCounts(const std::vector<unsigned int>& counts) {m_adc_counts = counts;}

  ///////////////////////////////////////////////////////////////////
  // Private data:
  ///////////////////////////////////////////////////////////////////
private:
  uint32_t m_board_id;
  bool     m_board_fail_flag;
  uint16_t m_pattern_trig_options;
  uint16_t m_channel_mask;
  uint32_t m_event_counter;
  uint32_t m_trigger_time_tag;
      
  unsigned int m_samples;
  unsigned int m_channel;
  std::vector<unsigned int> m_adc_counts;

  unsigned int m_ID;
};


///////////////////////////////////////////////////////////////////
// Inline methods:
///////////////////////////////////////////////////////////////////

inline unsigned int
ScintWaveform::channel() const { return m_channel; }

inline unsigned int 
ScintWaveform::board_id() const { return m_board_id; }

inline bool 
ScintWaveform::board_fail_flag() const { return m_board_fail_flag; }

inline unsigned int 
ScintWaveform::pattern_trig_options() const { return m_pattern_trig_options; }

inline unsigned int 
ScintWaveform::channel_mask() const { return m_channel_mask; }

inline unsigned int 
ScintWaveform::event_counter() const { return m_event_counter; }

inline unsigned int 
ScintWaveform::trigger_time_tag() const { return m_trigger_time_tag; }

inline unsigned int 
ScintWaveform::n_samples() const { return m_samples; }

inline const std::vector<unsigned int>&
ScintWaveform::adc_counts() const { return m_adc_counts; }

inline unsigned int 
ScintWaveform::identify() const { return m_ID; }

std::ostream 
&operator<<(std::ostream &out, const ScintWaveform &wfm);

CLASS_DEF( ScintWaveform, 193792541, 1 )

#endif // SCINTRAWEVENT_SCINTWAVEFORM_H

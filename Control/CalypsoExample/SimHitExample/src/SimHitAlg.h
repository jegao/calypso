#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "GeneratorObjects/McEventCollection.h"
#include "TrackerSimEvent/FaserSiHitCollection.h"
#include <TH1.h>

/* SimHit reading example - Ryan Rice-Smith, UC Irvine */

class SimHitAlg : public AthHistogramAlgorithm
{
    public:
    SimHitAlg(const std::string& name, ISvcLocator* pSvcLocator);

    virtual ~SimHitAlg();

    StatusCode initialize();
    StatusCode execute();
    StatusCode finalize();

    private:
    TH1* m_hist;  // Example histogram
    TH2* m_module;
    TH2* m_moduleSide1;
    TH2* m_moduleSide2;

    // Read handle keys for data containers
    // Any other event data can be accessed identically
    // Note the key names ("GEN_EVENT" or "SCT_Hits") are Gaudi properties and can be configured at run-time
    SG::ReadHandleKey<McEventCollection> m_mcEventKey       { this, "McEventCollection", "GEN_EVENT" };
    SG::ReadHandleKey<FaserSiHitCollection> m_faserSiHitKey { this, "FaserSiHitCollection", "SCT_Hits" };
};
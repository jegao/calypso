# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.AthConfigFlags import AthConfigFlags

def createGeoModelConfigFlags():
    gcf=AthConfigFlags()
    gcf.addFlag("GeoModel.FaserVersion", "FASER-01") #
    gcf.addFlag("GeoModel.GeoExportFile","")

    return gcf

#!/bin/env python


sctChannels = [
       '2147483648', '2149580800', '2151677952', '2153775104', '2155872256', '2157969408', '2160066560', '2162163712', '2164260864', '2166358016', '2168455168', '2170552320', '2172649472', '2174746624', '2176843776', '2178940928', '2181038080', '2183135232', '2185232384', '2187329536',
       '2189426688', '2191523840', '2193620992', '2195718144', '2197815296', '2199912448', '2202009600', '2204106752', '2206203904', '2208301056', '2210398208', '2212495360', '2214592512', '2216689664', '2218786816', '2220883968', '2222981120', '2225078272', '2227175424', '2229272576',
       '2231369728', '2233466880', '2235564032', '2237661184', '2239758336', '2241855488', '2243952640', '2246049792', '2281701376', '2283798528', '2285895680', '2287992832', '2290089984', '2292187136', '2294284288', '2296381440', '2298478592', '2300575744', '2302672896', '2304770048',
       '2306867200', '2308964352', '2311061504', '2313158656', '2315255808', '2317352960', '2319450112', '2321547264', '2323644416', '2325741568', '2327838720', '2329935872', '2332033024', '2334130176', '2336227328', '2338324480', '2340421632', '2342518784', '2344615936', '2346713088',
       '2348810240', '2350907392', '2353004544', '2355101696', '2357198848', '2359296000', '2361393152', '2363490304', '2365587456', '2367684608', '2369781760', '2371878912', '2373976064', '2376073216', '2378170368', '2380267520', '2415919104', '2418016256', '2420113408', '2422210560',
       '2424307712', '2426404864', '2428502016', '2430599168', '2432696320', '2434793472', '2436890624', '2438987776', '2441084928', '2443182080', '2445279232', '2447376384', '2449473536', '2451570688', '2453667840', '2455764992', '2457862144', '2459959296', '2462056448', '2464153600',
       '2466250752', '2468347904', '2470445056', '2472542208', '2474639360', '2476736512', '2478833664', '2480930816', '2483027968', '2485125120', '2487222272', '2489319424', '2491416576', '2493513728', '2495610880', '2497708032', '2499805184', '2501902336', '2503999488', '2506096640',
       '2508193792', '2510290944', '2512388096', '2514485248']


description = '<timeStamp>run-lumi</timeStamp><addrHeader><address_header clid="1238547719" service_type="71" /></addrHeader><typeName>CondAttrListCollection</typeName>'

descriptionDCS = '<timeStamp>time</timeStamp><addrHeader><address_header service_type="71" clid="1238547719" /></addrHeader><typeName>CondAttrListCollection</typeName><cache>600</cache>'

from PyCool import cool, coral

dbSvc = cool.DatabaseSvcFactory.databaseService()
connectString = 'sqlite://;schema=ALLP200.db;dbname=OFLP200'

print('recreating database')
dbSvc.dropDatabase( connectString )
db = dbSvc.createDatabase( connectString )

gainSpec = cool.RecordSpecification()
gainSpec.extend( 'serialNumber'          , cool.StorageType.UInt63 )
gainSpec.extend( 'runNumber'             , cool.StorageType.UInt32 )
gainSpec.extend( 'scanNumber'            , cool.StorageType.UInt32 )
gainSpec.extend( 'gainByChip'            , cool.StorageType.String4k )
gainSpec.extend( 'gainRMSByChip'         , cool.StorageType.String4k )
gainSpec.extend( 'offsetByChip'          , cool.StorageType.String4k )
gainSpec.extend( 'offsetRMSByChip'       , cool.StorageType.String4k )
gainSpec.extend( 'noiseByChip'           , cool.StorageType.String4k )
gainSpec.extend( 'noiseRMSByChip'        , cool.StorageType.String4k )

gainRecord = cool.Record(gainSpec)
gainRecord[ 'serialNumber'    ] = 0
gainRecord[ 'runNumber'       ] = 0
gainRecord[ 'scanNumber'      ] = 0
gainRecord[ 'gainByChip'      ] = '52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0 52.0'
gainRecord[ 'gainRMSByChip'   ] = '1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25 1.25'
gainRecord[ 'offsetByChip'    ] = '45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0'
gainRecord[ 'offsetRMSByChip' ] = '1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75 1.75'
gainRecord[ 'noiseByChip'     ] = '1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0 1600.0'
gainRecord[ 'noiseRMSByChip'  ] = '45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0 45.0'

gainFolderSpec = cool.FolderSpecification(gainSpec)
gainFolder = db.createFolder('/SCT/DAQ/Calibration/ChipGain', gainFolderSpec, description, True)

for channel in sctChannels:
    gainFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, gainRecord, int(channel) )

noiseSpec = cool.RecordSpecification()
noiseSpec.extend( 'serialNumber'          , cool.StorageType.UInt63 )
noiseSpec.extend( 'runNumber'             , cool.StorageType.UInt32 )
noiseSpec.extend( 'scanNumber'            , cool.StorageType.UInt32 )
noiseSpec.extend( 'offsetByChip'          , cool.StorageType.String4k )
noiseSpec.extend( 'occupancyByChip'       , cool.StorageType.String4k )
noiseSpec.extend( 'occupancyRMSByChip'    , cool.StorageType.String4k )
noiseSpec.extend( 'noiseByChip'           , cool.StorageType.String4k )

noiseRecord = cool.Record(noiseSpec)
noiseRecord[ 'serialNumber'       ] = 0
noiseRecord[ 'runNumber'          ] = 0
noiseRecord[ 'scanNumber'         ] = 0
noiseRecord[ 'offsetByChip'       ] = '60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0 60.0'
noiseRecord[ 'occupancyByChip'    ] = '3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05 3.50e-05'
noiseRecord[ 'occupancyRMSByChip' ] = '2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05 2.50e-05'

noiseFolderSpec = cool.FolderSpecification(noiseSpec)
noiseFolder = db.createFolder('/SCT/DAQ/Calibration/ChipNoise', noiseFolderSpec, description, True)

for channel in sctChannels:
    noiseFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, noiseRecord, int(channel) )

chanstatSpec = cool.RecordSpecification()
chanstatSpec.extend( 'LVCHSTAT_RECV' , cool.StorageType.Int32 )
chanstatSpec.extend( 'STATE'         , cool.StorageType.UInt32 )

chanstatRecord = cool.Record(chanstatSpec)
chanstatRecord[ 'LVCHSTAT_RECV' ] = 209
chanstatRecord[ 'STATE' ] = 17

chanstatFolderSpec = cool.FolderSpecification(chanstatSpec)
chanstatFolder = db.createFolder('/SCT/DCS/CHANSTAT', chanstatFolderSpec, descriptionDCS, True)

for channel in sctChannels:
    chanstatFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, chanstatRecord, int(channel) )

hvSpec = cool.RecordSpecification()
hvSpec.extend( 'HVCHVOLT_RECV' , cool.StorageType.Float )
hvSpec.extend( 'HVCHCURR_RECV' , cool.StorageType.Float )

hvRecord = cool.Record(hvSpec)
hvRecord[ 'HVCHVOLT_RECV' ] = 150.0
hvRecord[ 'HVCHCURR_RECV' ] = 10.0

hvFolderSpec = cool.FolderSpecification(hvSpec)
hvFolder = db.createFolder('/SCT/DCS/HV', hvFolderSpec, descriptionDCS, True)

for channel in sctChannels:
    hvFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, hvRecord, int(channel) )

modtempSpec = cool.RecordSpecification()
modtempSpec.extend( 'MOCH_TM0_RECV' , cool.StorageType.Float )
modtempSpec.extend( 'MOCH_TM1_RECV' , cool.StorageType.Float )

modtempRecord = cool.Record(modtempSpec)
modtempRecord[ 'MOCH_TM0_RECV' ] = 7.0
modtempRecord[ 'MOCH_TM1_RECV' ] = 7.0

modtempFolderSpec = cool.FolderSpecification(modtempSpec)
modtempFolder = db.createFolder('/SCT/DCS/MODTEMP', modtempFolderSpec, descriptionDCS, True)

for channel in sctChannels:
    modtempFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, modtempRecord, int(channel) )

mapSpec = cool.RecordSpecification()
mapSpec.extend( 'FieldType', cool.StorageType.String4k )
mapSpec.extend( 'MapFileName', cool.StorageType.String4k )

mapRecord = cool.Record(mapSpec)
mapRecord['FieldType'] = "GlobalMap"
mapRecord['MapFileName'] = "file:MagneticFieldMaps/FaserFieldTable.root"

mapFolderSpec = cool.FolderSpecification(mapSpec)
mapFolder = db.createFolder('/GLOBAL/BField/Maps', mapFolderSpec, descriptionDCS, True)

mapFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, mapRecord, 1 )

scaleSpec = cool.RecordSpecification()
scaleSpec.extend( 'value', cool.StorageType.Float )

scaleRecord = cool.Record(scaleSpec)
scaleRecord['value'] = 1.0

scaleFolderSpec = cool.FolderSpecification(scaleSpec)
scaleFolder = db.createFolder('/GLOBAL/BField/Scales', scaleFolderSpec, descriptionDCS, True)

# Channel names don't seem to be handled properly by Athena
scaleFolder.createChannel( 1, "Dipole_Scale" )
scaleFolder.storeObject( cool.ValidityKeyMin, cool.ValidityKeyMax, scaleRecord, 1 )

db.closeDatabase()
